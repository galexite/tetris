#include <ncurses.h>
#if (defined(__unix__) && !defined(__linux__)) || defined(__APPLE__)
#	include <stdlib.h>
#else
#	include <bsd/stdlib.h>
#endif
#include <stdio.h>
#include <string.h>
#include <limits.h>

#define TETRIS_OK 0
#define TETRIS_ERR 1
#define TETRIS_CONT 2
#define TETRIS_NEW 3

#define GRID_ROWS 20
#define GRID_COLS 10

#define DELTA 10

#define WORKING_TET_CHAR '@'

#define SPACING '.'

#define NUM_TYPES 7

#define MAX_NAME_LEN 21

#define SCORE_FILE ".scores"
#define MAX_SCORES 5

#define MAX_SCORE_DIGITS ((size_t) (1 + 0.30103 * sizeof(int) * CHAR_BIT))

/*
 * Template for x and y coordinates
 * as well as the characters displayed
 * for each tetromino type
 */
int template_x[NUM_TYPES][4] = {
        {0, 0, 0, 0},
        {0, 0, 1, 1},
        {0, 0, 0, 1},
        {-1, 0, 0, 0},
        {-1, 0, 0, 1},
        {-1, 0, 0, 1},
        {-1, 0, 0, 1}
};

int template_y[NUM_TYPES][4] = {
        {-1, 0, 1, 2},
        {0, 1, 0, 1},
        {0, 1, 2, 0},
        {0, 0, 1, 2},
        {0, 0, 1, 1},
        {1, 1, 0, 0},
        {1, 0, 1, 1}
};

int template_char[NUM_TYPES] = {
        'I',
        'O',
        'J',
        'L',
        'Z',
        'S',
        'T'
};

/* stores info about a tetromino */
struct tetromino
{
       int blocks_x[4];
       int blocks_y[4];
       unsigned int set_x;
       unsigned int set_y;
       char display_char;
};

/* Classic XOR swap */
void
swap_int(a, b)
int *a;
int *b;
{
        *a ^= *b;
        *b ^= *a;
        *a ^= *b;
}

/* swaps two blocks in a struct tetromino (vestigial) */
void
swap_blocks(tet, i, j)
struct tetromino *tet;
size_t i;
size_t j;
{
        swap_int(tet->blocks_x + i, tet->blocks_x + j);
        swap_int(tet->blocks_y + i, tet->blocks_y + j);
}

/* sorts blocks in a struct tetromino by y coord (vestigial) */
void
sort_blocks(tet)
struct tetromino *tet;
{
        int i;
        for (i = 0; i < 4; i += 2)
        {
                if ((tet->blocks_y)[i] > (tet->blocks_y)[i + 1])
                {
                        swap_blocks(tet, i, i + 1);
                }
        }

        if ((tet->blocks_y)[0] > (tet->blocks_y)[2])
        {
                swap_blocks(tet, 0, 2);
                swap_blocks(tet, 1, 3);
        }

        if ((tet->blocks_y)[1] > (tet->blocks_y)[2])
        {
                swap_blocks(tet, 1, 2);
                
                if ((tet->blocks_y)[2] > (tet->blocks_y)[3])
                {
                        swap_blocks(tet, 2, 3);
                }
        }
}

/* rotations of a struct tetromino by an angle counter-clockwise */
void
tetromino_r90(tet)
struct tetromino *tet;
{
        int i;
        for (i = 0; i < 4; ++i)
        {
                swap_int(tet->blocks_x + i, tet->blocks_y + i);
                (tet->blocks_x)[i] *= -1;
        }
        /* sort_blocks(tet); */
}

void
tetromino_r270(tet)
struct tetromino *tet;
{
        int i;
        for (i = 0; i < 4; ++i)
        {
                swap_int(tet->blocks_x + i, tet->blocks_y + i);
                (tet->blocks_y)[i] *= -1;
        }
        /* sort_blocks(tet); */
}

void
tetromino_r180(tet)
struct tetromino *tet;
{
        int i;
        for (i = 0; i < 2; ++i)
        {
                /* swap_blocks(tet, i, 4 - i); */
                (tet->blocks_x)[i] *= -1;
                (tet->blocks_x)[3 - i] *= -1;
                (tet->blocks_y)[i] *= -1;
                (tet->blocks_y)[3 - i] *= -1;
        }
}

/*
 * checks if a struct tetromino has blocks that
 * are in the same coordinates as blocks in the grid
 */
int
check_collisions(tet, grid)
struct tetromino *tet;
char grid[GRID_ROWS][GRID_COLS];
{
        int i;
        for (i = 0; i < 4; ++i)
        {
                int real_x = tet->set_x + tet->blocks_x[i];
                int real_y = tet->set_y + tet->blocks_y[i];
                if (grid[real_y][real_x] != SPACING)
                {
                        return 1;
                }
        }
        return 0;
}

/* checks if a struct tetromino has out-of-bounds blocks */
int
check_bounds(tet)
struct tetromino *tet;
{
        int i;
        for (i = 0; i < 4; ++i)
        {
                int real_x = tet->set_x + tet->blocks_x[i];
                int real_y = tet->set_y + tet->blocks_y[i];
                if (real_x < 0 ||
                    real_y < 0 ||
                    real_x >= GRID_COLS ||
                    real_y >= GRID_ROWS)
                {
                        return 1;
                }
        }
        return 0;
}

void
tetromino_r_rand(tet)
struct tetromino *tet;
{
        int rot = arc4random_uniform(4);
        switch (rot)
        {
                case 1:
                        tetromino_r90(tet);
                        break;
                case 2:
                        tetromino_r180(tet);
                        break;
                case 3:
                        tetromino_r270(tet);
                        break;
        }
}

void
tet_fix_bounds(tet)
struct tetromino *tet;
{
        int i;
        for (i = 0; i < 4; ++i)
        {
                int real_x = tet->blocks_x[i] + tet->set_x;
                int real_y = tet->blocks_y[i] + tet->set_y;
                if (real_y < 0)
                {
                        tet->set_y -= real_y;
                }
                if (real_x < 0)
                {
                        tet->set_x -= real_x;
                }
                if (real_x >= GRID_COLS)
                {
                        tet->set_x -= real_x - GRID_COLS + 1;
                }
        }

}

void
new_tetromino(tet)
struct tetromino *tet;
{
        int type = arc4random_uniform(NUM_TYPES);
        tet->set_x = arc4random_uniform(GRID_COLS);
        tet->set_y = 0;
        memcpy(tet->blocks_x,
               template_x[type], /*TODO*/
               sizeof(int) * 4);
        memcpy(tet->blocks_y,
               template_y[type],
               sizeof(int) * 4);
        tet->display_char = template_char[type];
        tetromino_r_rand(tet);
        tet_fix_bounds(tet);
}

int
init_curses(void)
{
        initscr();
        cbreak();
        keypad(stdscr, TRUE);
        noecho();
        halfdelay(DELTA);
        return TETRIS_OK;
}

int
kill_curses(void)
{
        endwin();
        return TETRIS_OK;
}

void
del_line(grid, row)
char grid[GRID_ROWS][GRID_COLS];
int row;
{
        int i;
        for (i = row; i >= 0; --i)
        {
                int j;
                for (j = 0; j < GRID_COLS; ++j)
                {
                        if (i > 0)
                        {
                                grid[i][j] = grid[i - 1][j];
                        }
                        else
                        {
                                grid[i][j] = SPACING;
                        }
                }
        }
}

void
del_full_lines(score_ptr, grid)
int *score_ptr;
char grid[GRID_ROWS][GRID_COLS];
{
        int i;
        for (i = 0; i < GRID_ROWS; ++i)
        {
                int full = 1;
                int j;
                for (j = 0; j < GRID_COLS; ++j)
                {
                        full = (full && (grid[i][j] != SPACING));
                }
                if (full)
                {
                        del_line(grid, i);
                        ++(*score_ptr);
                }
        }
}

void
add_tet_grid(tet, grid, disp_char)
struct tetromino *tet;
char grid[GRID_ROWS][GRID_COLS];
char disp_char;
{
        int i;
        for (i = 0; i < 4; ++i)
        {
                int real_x = tet->set_x + tet->blocks_x[i];
                int real_y = tet->set_y + tet->blocks_y[i];
                grid[real_y][real_x] = disp_char;
        }
}

void
tetris_up(tet, grid)
struct tetromino *tet;
char grid[GRID_ROWS][GRID_COLS];
{
        tetromino_r90(tet);
        if (check_bounds(tet) || check_collisions(tet, grid))
        {
                tetromino_r270(tet);
        }
}

void
tetris_left(tet, grid)
struct tetromino *tet;
char grid[GRID_ROWS][GRID_COLS];
{
        --(tet->set_x);
        if (check_bounds(tet) || check_collisions(tet, grid))
        {
                ++(tet->set_x);
        }
}

void
tetris_right(tet, grid)
struct tetromino *tet;
char grid[GRID_ROWS][GRID_COLS];
{
        ++(tet->set_x);
        if (check_bounds(tet) || check_collisions(tet, grid))
        {
                --(tet->set_x);
        }
}

int
tetris_down(tet, next_tet, grid)
struct tetromino *tet;
struct tetromino *next_tet;
char grid[GRID_ROWS][GRID_COLS];
{
        tet->set_y++;
        if (check_bounds(tet) || check_collisions(tet, grid))
        {
                tet->set_y--;
                add_tet_grid(tet, grid, tet->display_char);
                memcpy(tet, next_tet, sizeof(struct tetromino));
                new_tetromino(next_tet);
                if (check_collisions(tet, grid))
                {
                        return TETRIS_OK;
                }
                return TETRIS_NEW;
        }
        return TETRIS_CONT;
}

int
tetris_update(grid, score_ptr, tet, next_tet)
char grid[GRID_ROWS][GRID_COLS];
int *score_ptr;
struct tetromino *tet;
struct tetromino *next_tet;
{
        int in_key = getch();
        int status = TETRIS_CONT;
        int new_tet = 0;
        switch (in_key)
        {
                case KEY_UP:
                        tetris_up(tet, grid);
                        break;
                case KEY_DOWN:
                        status = tetris_down(tet, next_tet, grid);
                        if (status == TETRIS_NEW)
                        {
                                new_tet = 1;
                        }
                        break;
                case KEY_LEFT:
                        tetris_left(tet, grid);
                        break;
                case KEY_RIGHT:
                        tetris_right(tet, grid);
                        break;
        }
        if (!new_tet)
        {
                status = tetris_down(tet, next_tet, grid);
        }
        if (status == TETRIS_OK)
        {
                return TETRIS_OK;
        }
        del_full_lines(score_ptr, grid);
        return TETRIS_CONT;
}

void
clone_grid(dest, src)
char dest[GRID_ROWS][GRID_COLS];
char src[GRID_ROWS][GRID_COLS];
{
        int i;
        for (i = 0; i < GRID_ROWS; ++i)
        {
                int j;
                for (j = 0; j < GRID_COLS; ++j)
                {
                        dest[i][j] = src[i][j];
                }
        }
}

int
tetris_draw(grid, score, curr_tet, next_tet)
char grid[GRID_ROWS][GRID_COLS];
int score;
struct tetromino *curr_tet;
struct tetromino *next_tet;
{
        char temp_grid[GRID_ROWS][GRID_COLS];
        int i;
        clone_grid(temp_grid, grid);
        add_tet_grid(curr_tet, temp_grid, WORKING_TET_CHAR);
        erase();
        for (i = 0; i < GRID_ROWS; ++i)
        {
                move(i, 0);
                addnstr(temp_grid[i], GRID_COLS);
        }
        move(0, GRID_COLS + 1);
        printw("Score: %d", score);
        for (i = 0; i < 4; ++i)
        {
                move(5 + next_tet->blocks_y[i],
                     GRID_COLS + 5 + next_tet->blocks_x[i]);
                addch(next_tet->display_char);
        }
        refresh();
        return TETRIS_OK;
}

void
init_grid(grid)
char grid[GRID_ROWS][GRID_COLS];
{
        int i;
        for (i = 0; i < GRID_ROWS; ++i)
        {
                int j;
                for (j = 0; j < GRID_COLS; ++j)
                {
                        grid[i][j] = SPACING;
                }
        }
}

int
tetris_loop(void)
{
        char grid[GRID_ROWS][GRID_COLS];
        init_grid(grid);
        int score = 0;
        int keep_going = TETRIS_CONT;
        struct tetromino curr_tet;
        struct tetromino next_tet;
        int status;
        new_tetromino(&curr_tet);
        new_tetromino(&next_tet);
        status = tetris_draw(grid, score, &curr_tet, &next_tet);
        if (status == TETRIS_ERR)
        {
                return -1;
        }
        while (keep_going == TETRIS_CONT)
        {
                keep_going = tetris_update(grid, &score, &curr_tet, &next_tet);
                status = tetris_draw(grid, score, &curr_tet, &next_tet);
                if (status == TETRIS_ERR)
                {
                        return -1;
                }
        }
        return score;
}

struct score_record
{
        char name[MAX_NAME_LEN];
        int score;
};

int
next_line(file)
FILE *file;
{
        int c;
        for (c = getc(file); c != '\n' && c != EOF; c = getc(file)) ;
        c = getc(file);
        if (c == EOF)
        {
                return EOF;
        }
        ungetc(c, file);
        return '\n';
}

int
get_score(score_file)
FILE *score_file;
{
        char score_str[MAX_SCORE_DIGITS + 1];
        if (fgets(score_str, MAX_SCORE_DIGITS + 1, score_file) == NULL)
        {
                return -1;
        }
        return atoi(score_str);
}

int
parse_score_file(scores)
struct score_record scores[MAX_SCORES];
{
        FILE *score_file = fopen(SCORE_FILE, "r");
        size_t i;
        int c = 0;
        if (score_file == NULL)
        {
                return 0;
        }
        i = 0;
        while (c != EOF)
        {
                scores[i].score = get_score(score_file);
                if (scores[i].score < 0)
                {
                        return -1; /* TODO cleanup */
                }
                c = getc(score_file);
                if (c == EOF)
                {
                        return -1; /* TODO cleanup */
                }
                if (fgets(scores[i].name, MAX_NAME_LEN, score_file) == NULL)
                {
                        return -1; /* TODO cleanup */
                }
                c = next_line(score_file);
                ++i;
        }
        return i;
}

unsigned int
update_leaderboard(scores, new_score, name, s_size)
struct score_record scores[MAX_SCORES];
int new_score;
char name[MAX_NAME_LEN];
int s_size;
{
        size_t i = 0;
        size_t pos;
        if (scores == NULL)
        {
                return 0;
        }
        while (scores[i].score > new_score && i < s_size)
        {
                ++i;
        }
        pos = i;
        if (pos < MAX_SCORES)
        {
                i = MAX_SCORES - 1;
                while (i > pos)
                {
                        scores[i] = scores[i - 1];
                        --i;
                }
                scores[pos].score = new_score;
                memcpy(scores[pos].name, name, MAX_NAME_LEN);
        }
        return pos + 1;
}

void
print_leaderboard(scores, position, s_size)
struct score_record scores[MAX_SCORES];
unsigned int position;
int s_size;
{
        size_t i;
        printf("#\tName");
        for (i = 0; i < MAX_NAME_LEN - 4; ++i)
        {
                putchar(' ');
        }
        printf("Score\n");
        for (i = 1; i <= s_size; ++i)
        {
                printf("%lu%c\t%s %d\n",
                       i,
                       i == position ? '*' : ' ',
                       scores[i - 1].name,
                       scores[i - 1].score);
        }
}

int
update_score_file(scores, s_size)
struct score_record scores[MAX_SCORES];
int s_size;
{
        FILE *score_file = fopen(SCORE_FILE, "w+");
        size_t i;
        if (score_file == NULL)
        {
                return TETRIS_ERR;
        }
        for (i = 0; i < s_size; ++i)
        {
                fprintf(score_file,
                        "%0*d %s\n",
                        (int) MAX_SCORE_DIGITS,
                        scores[i].score,
                        scores[i].name);
        }
        if (fclose(score_file) == EOF)
        {
                return TETRIS_ERR;
        }
        return TETRIS_OK;
}

void
rightpad_name(name)
char name[MAX_NAME_LEN];
{
        size_t i = 0;
        while (name[i] != '\n')
        {
                ++i;
        }
        while (i < MAX_NAME_LEN - 1)
        {
                name[i] = ' ';
                ++i;
        }
        name[i] = '\0';
}

int
update_scores(score)
int score;
{
        char name[MAX_NAME_LEN];
        struct score_record scores[MAX_SCORES];
        unsigned int position;
        int leaderboard_size;
        printf("Please enter your name (Max %d characters): ", MAX_NAME_LEN - 1);
        if (fgets(name, MAX_NAME_LEN, stdin) == NULL)
        {
                return TETRIS_ERR;
        }
        rightpad_name(name);
        leaderboard_size = parse_score_file(scores);
        if (leaderboard_size < 0)
        {
                return TETRIS_ERR;
        }
        position = update_leaderboard(scores, score, name, leaderboard_size);
        if (position == 0)
        {
                return TETRIS_ERR;
        }
        else if (position > MAX_SCORES)
        {
                printf("Unfortunately, you did not make the top %d.\n",
                       MAX_SCORES);
                print_leaderboard(scores, 0, leaderboard_size);
                printf("*\t%s %d\n", name, score);
        }
        else
        {
                printf("Congratulations, you made the top %d!\n",
                       MAX_SCORES);
                if (leaderboard_size < MAX_SCORES)
                {
                        ++leaderboard_size;
                }
                print_leaderboard(scores, position, leaderboard_size);
                if (update_score_file(scores, leaderboard_size) ==
                    TETRIS_ERR)
                {
                        return TETRIS_ERR;
                }
        }
        return TETRIS_OK;
}

int
main(argc, argv)
int argc;
char **argv;
{
        int status;
        int keep_going = 0;
        int score;
        status = init_curses();
        if (status == TETRIS_ERR)
        {
                return EXIT_FAILURE;;
        }
        score = tetris_loop();
        if (score < 0)
        {
                kill_curses();
                return EXIT_FAILURE;
        }
        status = kill_curses();
        if (status == TETRIS_ERR)
        {
                return EXIT_FAILURE;
        }
        status = update_scores(score);
        if (status == TETRIS_ERR)
        {
                return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
}
